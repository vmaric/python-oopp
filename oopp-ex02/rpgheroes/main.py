import dwarf

dwarfWarrior = dwarf.DwarfWarrior()
dwarfWarrior.dead = False
dwarfWarrior.health = 25
dwarfWarrior.mana = 25
dwarfWarrior.show()
dwarfWarrior.receiveHit()
dwarfWarrior.show()
dwarfWarrior.receiveHit()
dwarfWarrior.show()
dwarfWarrior.primaryFire()
dwarfWarrior.show()
dwarfWarrior.secondaryFire()
dwarfWarrior.show()
dwarfWarrior.receiveHit()
dwarfWarrior.show()

